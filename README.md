# Avaliação Técnica - Grupo Dimed - Frontend

## Executando o Projeto

Para executar o projeto basta realizar os seguintes passos:

```bash
# Clone o repositório
$ git clone https://gitlab.com/nikolaslacerda/avaliacao-tecnica-frontend.git

# Entre na pasta do projeto
$ cd ./avaliacao-tecnica-frontend/frontend 

# Execute o projeto
$ docker-compose up

```

O projeto será iniciado no seguinte caminho: http://localhost:8081/
