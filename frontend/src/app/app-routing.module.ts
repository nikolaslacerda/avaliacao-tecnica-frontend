import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItinerarioComponent } from './pages/itinerario/itinerario.component';
import { ListaLinhasComponent } from './pages/lista-linhas/lista-linhas.component';

const routes: Routes = [
  { path: '', component: ListaLinhasComponent},
  { path: 'linhas', component: ListaLinhasComponent},
  { path: 'itinerario/:id', component: ItinerarioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
