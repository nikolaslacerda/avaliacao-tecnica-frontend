import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faMapMarkerAlt, faAngleDoubleUp, faMap } from '@fortawesome/free-solid-svg-icons';
import { Itinerario } from 'src/app/shared/models/itinerario.model';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

  id: number = -1;
  faMapMarkerAlt = faMapMarkerAlt
  faMap = faMap
  faAngleDoubleUp = faAngleDoubleUp
  itinerario = {} as Itinerario;
  isShow: boolean = false;
  topPosToStartShowing = 500;

  constructor(private route: ActivatedRoute, private service: ApiService) { }

  ngOnInit(): void {
    this.service.backButton = true
    this.id = this.route.snapshot.params.id;
    this.getItinerario()
  }

  getItinerario() {
    this.service
      .getItinerario(this.id)
      .subscribe((json: any) => {
        this.itinerario.codigo = json.codigo;
        this.itinerario.nome = json.nome;
        this.itinerario.idlinha = json.idlinha;

        delete json.codigo;
        delete json.nome;
        delete json.idlinha;

        this.itinerario.pontos = Object.keys(json).map(i => json[i]);
      });
  }

  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }
}
