import { Component, HostListener, OnInit } from '@angular/core';
import { Linha } from 'src/app/shared/models/linha.model';
import { ApiService } from 'src/app/shared/services/api.service';
import { faBus, faShuttleVan, faAngleRight, faSearch, faAngleDoubleUp } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-linhas',
  templateUrl: './lista-linhas.component.html',
  styleUrls: ['./lista-linhas.component.css']
})
export class ListaLinhasComponent implements OnInit {

  linhas = [] as Array<Linha>;
  lotacoes = [] as Array<Linha>;
  searchText: string = "";
  contentLoaded: boolean = false;
  faBus = faBus
  faSearch = faSearch
  faAngleDoubleUp = faAngleDoubleUp
  faAngleRight = faAngleRight
  faShuttleVan = faShuttleVan
  count: number = 5
  isShow: boolean = false;
  topPosToStartShowing = 500;

  constructor(private router: Router, private service: ApiService) { }

  ngOnInit(): void {
    this.service.backButton = false
    this.getAllOnibus()
    this.getAllLotacao()
  }

  getAllOnibus() {
    this.service.getAllOnibus().subscribe((linhas: Array<Linha>) => {
      this.linhas = linhas;
      this.contentLoaded = true;
    }, error => console.log(error)
    );
  }

  getAllLotacao() {
    this.service.getAllLotacao().subscribe((linhas: Array<Linha>) => {
      this.lotacoes = linhas;
    }, error => console.log(error)
    );
  }

  goToItinerario(linha: Linha) {
    this.router.navigateByUrl('/itinerario/' + linha.id);
  }

  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

}
