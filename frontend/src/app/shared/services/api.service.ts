import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Linha } from '../models/linha.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public backButton: boolean = false;

  constructor(private http: HttpClient) { }

  getAllOnibus(): Observable<Array<Linha>> {
    return this.http.get<Array<Linha>>('http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o');
  }

  getAllLotacao(): Observable<Array<Linha>> {
    return this.http.get<Array<Linha>>('http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l');
  }

  getItinerario(id: number): Observable<any> {
    return this.http.get<any>('http://www.poatransporte.com.br/php/facades/process.php?a=il&p=' + id);
  }



}
