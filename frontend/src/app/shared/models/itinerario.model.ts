export interface Itinerario {
  idlinha: string,
  codigo: string,
  nome: string,
  pontos: Array<Ponto>
}

export interface Ponto {
  lat: string
  lng: string
}
