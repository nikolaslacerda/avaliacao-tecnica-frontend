import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from 'src/app/shared/services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  faAngleLeft = faAngleLeft

  constructor(private location: Location, private service: ApiService) { }

  ngOnInit(): void {
  }

  back(): void {
    this.location.back()
  }

  isVisible(): boolean {
    return this.service.backButton;
  }

}
